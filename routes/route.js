'use strict'

const userFunction = require('../controller/controller')

module.exports = [
    {method: 'POST', path: '/lambda/create/user', config: userFunction.createUser},
    {method: 'POST', path: '/lambda/get/user', config: userFunction.getUsers},
    {method: 'PUT', path: '/lambda/update/user', config: userFunction.updateUser},
    {method: 'DELETE', path: '/lambda/delete/user', config: userFunction.deleteUser}
]