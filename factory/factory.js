'use strict'

var AWS = require("aws-sdk");

AWS.config.update({
  region: "ap-south-1"
});

exports.createUser = (request, h) => {

    const table = request.payload.table;
    const id = request.payload.id;
    const name = request.payload.name;

    var docClient = new AWS.DynamoDB.DocumentClient();

    var params = {
        TableName:table,
        Item:{
            "user_id": id,
            "user_name": name
        }
    };

    console.log("Adding a new item...");
    docClient.put(params, function(err, data) {
        if (err) {
            console.error("Unable to add item. Error JSON:", JSON.stringify(err, null, 2));
            return JSON.stringify(err, null, 2)
        } else {
            console.log("Added item:", JSON.stringify(data, null, 2));
            return data;
        }
    });

}


exports.getUsers = (request, h) => {

    const table = request.payload.table;
    var docClient = new AWS.DynamoDB.DocumentClient();

    var params = {
        TableName: table,
        ProjectionExpression: "user_id, user_name"
    };

    console.log("Scanning User table.");
    docClient.scan(params, onScan);

    function onScan(err, data) {
        if (err) {
            console.error("Unable to scan the table. Error JSON:", JSON.stringify(err, null, 2));
        } else {
            // print all the users
            console.log("Scan succeeded.");
            data.Items.forEach(function(user) {
            console.log(
                    user.user_id+ ": ",
                    user.user_name);
            });

            // continue scanning if we have more users, because
            // scan can retrieve a maximum of 1MB of data
            if (typeof data.LastEvaluatedKey != "undefined") {
                console.log("Scanning for more...");
                params.ExclusiveStartKey = data.LastEvaluatedKey;
                docClient.scan(params, onScan);
            }
        }
    }

  
}


exports.updateUser = (request, h) => {

    const table = request.payload.table;
    const id = request.payload.id;
    const name = request.payload.name;
    const newName = request.payload.newName;
    var docClient = new AWS.DynamoDB.DocumentClient();


    var params = {
        TableName: table,
        Key:{
            "user_id": id,
        },
        UpdateExpression: 'set user_name = :t',
        ExpressionAttributeValues: {
        ':t' : newName,
        }
    };
    
    docClient.update(params, function(err, data) {
        if (err) {
        console.log("Error", err);
        return err;
        } else {
        console.log("Success", data);
        return data;
        }
    });
  
}


exports.deleteUser = (request, h) => {

    const table = request.payload.table;
    const id = request.payload.id;
    const name = request.payload.name;
    var docClient = new AWS.DynamoDB.DocumentClient();


    var params = {
        TableName: table,
        Key:{
            "user_id": id,
        }
    };
    
    docClient.delete(params, function(err, data) {
        if (err) {
        console.log("Error", err);
        return err;
        } else {
        console.log("Success", data);
        return data;
        }
    });
  
}

