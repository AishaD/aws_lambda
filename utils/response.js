'use strict'

//RESPONSE FUNCTION
exports.responseFunction = (message, statusCode, result) => {

    // run "async"
    return {
        message,
        statusCode,
        result
    };
};
  
 