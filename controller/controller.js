'use strict'

const {createUser, getUsers, updateUser, deleteUser} = require('../factory/factory');
const {responseFunction} = require('../utils/response');
const config = require('../config/msg.json')

exports.createUser = {

    tags: ['api', 'Create User'],
    description: 'Create User',
    notes: 'Create User',
    handler: (request, h) => {

        return responseFunction(config.create, config.status, createUser(request));
    }
}

exports.getUsers = {

    tags: ['api', 'Get All Users'],
    description: 'Get All Users',
    notes: 'Get All Users',
    handler: (request, h) => {
        
        return responseFunction(config.display, config.status, getUsers(request));
    }
}

exports.updateUser = {

    tags: ['api', 'Update User'],
    description: 'Update User',
    notes: 'Update User',
    handler: (request, h) => {
        
        return responseFunction(config.update, config.status, updateUser(request));
    }
}

exports.deleteUser = {

    tags: ['api', 'Delete User'],
    description: 'Delete User',
    notes: 'Delete User',
    handler: (request, h) => {

        return responseFunction(config.delete, config.status, deleteUser(request));
    }
}